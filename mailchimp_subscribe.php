<?php
/**Если надо сюда данные для инициализации как плагина)***/

add_action('init', 'chimp_add_shortcode');
add_action('wp_ajax_chimp_send_ajax', 'CHIMP_CALLBACK');
add_action('wp_ajax_nopriv_chimp_send_ajax', 'CHIMP_CALLBACK');

function chimp_add_shortcode(){ 
    add_shortcode('chimpform', 'chimp_form_shortcode');

}

function chimp_form_shortcode($attrs){

	// Параметры для шорткода, переопредлишь в шорткоде)
	$params = shortcode_atts( array( 
		'api_key' => '',
		'list_id' => '',
		'placeholder' => 'Your E-Mail',
		'btntext' => 'Subscribe >',
		'success' => 'Congratulation!',
		'error' => 'Ooops! ERROR',
	), $attrs );

	if(empty($params['api_key'])) return 'Неверный API key';
	if(empty($params['list_id'])) return 'Неверный ID списка подписчиков';

	$nonce = wp_create_nonce('chimp_send_ajax_nonce');
    $url = admin_url('admin-ajax.php?action=chimp_send_ajax&nonce='.$nonce);

	$placeholder = $params['placeholder'];
	$btext = $params['btntext'];
	$apikey = $params['api_key'];
	$listId = $params['list_id'];
	$success = $params['success'];
	$error = $params['error'];

	$view = '<form id="chmForm-1" method="POST" action="/">
	<label>Подписать на рассылку:</label> <input name="mail" id="chimp_email" value="'.$email.'" placeholder="'.$placeholder.'"></input>
	<input type="submit" value="'.$btext.'" class="chimp_send" name="send"></input>
	<input type="hidden" name="chimp_ajax_form_nonce" value="'.$nonce.'" id="chimp_ajax_form_nonce"/>
	</form><div id="chimp_msg"></div>';

	/***********hardcode*************/
	$view .= "<script>jQuery(document).ready( function() {
	    jQuery('#chmForm-1 .chimp_send').click( function() {
		var apikey = '".$apikey."';
		var listId = '".$listId."';
		var mail = jQuery('#chimp_email').val();
		if(mail == '') {
		   jQuery('#chimp_msg').html('Заполните e-mail');
		   return false;
		}
	    var regex =  new RegExp(/^(('[\w-\s]+')|([\w-]+(?:\.[\w-]+)*)|('[\w-\s]+')([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i); 
		if(!regex.test(mail)){ 
 			jQuery('#chimp_msg').html('Некоректный e-mail');
		   	return false;
		} 

	    jQuery.ajax({
	        type : 'post',
	        dataType : 'json',
	        url : '".$url."',
	        data : {
			action: 'chimp_send_ajax',
			nonce : '".$nonce."',
			mail : mail,
			apikey : apikey,
			listId : listId
		},
		   error : function(request, txtstatus, errorThrown){
			console.log(request);
		   },
	         success : function(response) {
				console.log(response);
	            if(response.msg == 'success') {
	            	if(response.error) {
	            		jQuery('#chimp_msg').html(response.error);
			   		}else{ 
			   			jQuery('#chimp_msg').html('".$success."');
			   			jQuery('#chmForm-1').hide(50);
			   		}
	            }else{
	               jQuery('#chimp_msg').html('".$error."');
	            }
	         }
	      })
	      return false;   
	   });
	})</script>";
	/***********#hardcode*************/
	return $view;
}
 

function CHIMP_CALLBACK(){
 
	$result = array();
    
    if ( !wp_verify_nonce( $_REQUEST['nonce'], "chimp_send_ajax_nonce")) {
      exit("Fail nonce");
   	}
 
	if(!$_REQUEST['mail'] || !$_REQUEST['apikey'] || !$_REQUEST['listId']) { 
        $result['msg'] = "false";
        $result = json_encode($result);
        echo $result;
        wp_die();
    }

	$api_key = $_REQUEST['apikey'];
	$userId = explode('-',$api_key); //ИД юзера он в конце апикея через тире, надеюсь конструкция не поменяеться)
	$user = $userId[1];
	$list = $_REQUEST['listId'];//идишник списка для подписчиков на чимпе, вынес его в шорткоде тоже
	$mail = $_REQUEST['mail'];

	$url = "https://$user.api.mailchimp.com/2.0/lists/subscribe.json";

	$request = wp_remote_post( $url, array(
	    'body' => json_encode( array(
	        'apikey' => $api_key,
	        'id' => $list,
	        'email' => array( 'email' => $mail ),
	    ) ),
	) );

	$res = json_decode( wp_remote_retrieve_body( $request ) );

	if($res->code == 214) $result['error'] = $res->error; // Типа email уже есть... Остальные коды лень было отлавливать

	$result['msg'] = "success";
	$result = json_encode($result);

    echo $result;

	wp_die(); 
}
?>